

// MIT License, Free Software


/*
 cc uneml.c -o uneml 
 usage: uneml *.eml       this will create all the PDF files from all your eml files.
 tested with https://www.phpclasses.org/browse/download/1/file/14672/name/message.eml
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h> 
#include <time.h>
#if defined(__linux__) //linux
#define OSTYPE 1
#elif defined(_WIN32)
#define OSTYPE 9
#elif defined(_WIN64)
#define OSTYPE 8
#elif defined(__unix__) 
#define OSTYPE 2
#define PATH_MAX 2500
#else
#define OSTYPE 3
#endif


int mode_debug = 0; 
int filecounter = 1; 


void filecat( char *filein ) 
{
	int curreadpos;
	FILE *fp5;
	FILE *fp6;
	char strfooreadline[PATH_MAX];
	char strfooreadlinetmp[PATH_MAX];
	char fileout[PATH_MAX];
	int open_tab = 0; 

	fp6 = fopen( filein , "rb");
	printf( " Reading... \n" ); 
	while( !feof(fp6) ) 
	{
		fgets(strfooreadlinetmp, PATH_MAX, fp6); 
		strncpy( strfooreadline, "" , PATH_MAX );

		for( curreadpos = 0 ; ( curreadpos <= strlen( strfooreadlinetmp ) ); curreadpos++ )
			if ( strfooreadlinetmp[ curreadpos ] != '\n' )
				strfooreadline[curreadpos]=strfooreadlinetmp[curreadpos];


		if ( mode_debug == 1 ) printf( "(%s) (%d)\n" , strfooreadline, strfooreadline[0] ); 
		if ( strfooreadline[0]  == 13 ) 
		{
			if ( open_tab == 1 ) 
			{
				printf(  "Found beginning\n" ); 
				open_tab = 2; 
				fp5 = fopen( "target.dat" , "wb" ); 
				fclose( fp5 ); 
			}
			else if ( open_tab == 2 ) 
			{
				printf(  " Closing base64\n" ); 
				//nsystem( " tcbase64 -dec target.dat  output.pdf " ); 
				snprintf( fileout , sizeof(fileout), "tcbase64 -dec target.dat file-decoded-%d.pdf", filecounter++);
				system( fileout ); 
				open_tab = 0; 
			}
		}

		if ( open_tab == 2 )    
		{
			if ( strfooreadline[0]  != 13 ) 
			{
				printf( "IMAGE CONTENT: %s\n", strfooreadline ); 
				fp5 = fopen( "target.dat" , "ab" ); 
				fputs( strfooreadline, fp5 ); 
				fclose( fp5 ); 
			}
		}


		if (  strstr(  strfooreadline,  "Content-Transfer-Encoding: base64"  ) != 0 ) 
		{
			open_tab = 1; 
			printf( "Found content base64\n" ); 
		}

	}
	fclose( fp6 );
        open_tab = 0; 
}








int main( int argc, char *argv[])
{
    char cwd[PATH_MAX]; int i ; 

    if ( argc >= 2 )
    {
         for( i = 1  ; i <= argc-1 ; i++) 
	 {
	    printf( "\n[Read file eml: %s]\n" , argv[ i ] ); 
	    filecat( argv[i] ); 
	 }
    }
    return 0;
}



